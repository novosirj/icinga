#!/bin/bash

IPS="9.155.114.146 9.155.114.147 9.155.114.148 9.155.118.20 9.155.118.21 9.155.118.22"
FILENAME="check_spectrumscale.sh"
REMOTE_PATH="/usr/lib64/nagios/plugins/"

for i in ${IPS} ; do
  /usr/bin/scp ${FILENAME} root@${i}:${REMOTE_PATH}/
done
